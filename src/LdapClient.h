#pragma once
#include <string>
#include <string_view>

struct ldap;

namespace ldap_inj {
    
    class LdapClient {
        public:
            LdapClient() = default;
            ~LdapClient();
            /*
             * \brief connect to LDAP server 
             */
            bool connect(const std::string& host, int port);
            /*
             * \brief chech user login and password
             */            
            bool hasUser(const std::string& user, const std::string& password = {});
        private:
            struct ldap* m_handler;
    };

}