#include "AuthenticationApp.h"

int main() {
    ldap_inj::AuthenticationApp app;
    app.run();
    
    return 0;
}